const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

// const lightCss = new ExtractTextWebpackPlugin("light.css");
// const darkCss = new ExtractTextWebpackPlugin("dark.css");

module.exports = {
    entry: {
      'js/cover': './src/js/index.js',
      'css/cover': './src/sass/light/_all.sass',
      'css/cover-dark': './src/sass/dark/_all.sass'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [[
                            '@babel/preset-env',
                            {
                                targets: "> 2.5%, not dead",
                            },
                        ]]
                    }
                }
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({ filename: '[name].css', }),
        new CopyPlugin({patterns: [{ from: 'public', to: '' }]}),
        new CompressionPlugin({ exclude: /.+\.html/ })
    ],
};
