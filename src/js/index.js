import Bulma from '@vizuaalog/bulmajs';

import CoverDropdown from './plugins/cover-dropdown';
import CoverNavbar from './plugins/cover-navbar';
import CoverTabs from './plugins/cover-tabs';
import CoverTabsVertical from './plugins/cover-tabs-vertical';
import AutoPopup from './plugins/auto-popup';
import Collapse from './plugins/collapse';
import StickyObserver from './plugins/sticky-observer';
import ContentLoader from './plugins/content-loader';
import * as utils from './utils';

document.addEventListener('partial-content-loaded', event => Bulma.traverseDOM(event.detail));

export { 
    Bulma,
    AutoPopup,
    Collapse,
    ContentLoader,
    CoverDropdown,
    CoverNavbar,
    CoverTabs,
    CoverTabsVertical,
    StickyObserver,
    utils
};
