import Bulma from '@vizuaalog/bulmajs/src/core';
import {reflow} from '../utils';


class Collapse {
    /**
     * Get the root class this plugin is responsible for.
     * This will tell the core to match this plugin to an element with a .modal class.
     * @returns {string} The class this plugin is responsible for.
     */
    static getRootClass() {
        return 'collapse';
    }


    /**
     * Handle parsing the DOMs data attribute API.
     * @param {HTMLElement} element The root element for this instance
     * @return {undefined}
     */
    static parse(element) {
        new Collapse({
            element: element
        });
    }

    constructor(options) {
        this.element = options.element;
        this.content = options.element.querySelector('.collapse-content');
        this.trigger = options.element.querySelector('.collapse-trigger');

        // Set initial state
        this.isVisible = this.content.classList.contains('is-visible');
        this.content.setAttribute('aria-expanded', this.isVisible);

        // Detect transitions
        const styles = window.getComputedStyle(this.content);
        this.hasTransition = styles.transitionDelay !== "0s" || styles.transitionDuration !== "0s";

        this.setupEvents();
    }

    setupEvents() {
        // Don't need trigger for programmatic triggering
        if (this.trigger) {
            this.trigger.addEventListener('click', (event) => {
                event.preventDefault();
                this.toggle();
            });
        }
    }

    /**
     * Toggle .collapse-content based on current state
     */
    toggle() {
        if (this.isVisible)
            this.hide();
        else
            this.show();
    }

    /**
     * Show .collapse-content if not visible
     */
    show() {
        if (this.isVisible)
            return;

        if (this.hasTransition)
            this.handleTransitionIn();

        this.content.classList.add('is-visible');
        this.content.setAttribute('aria-expanded', true);
        this.isVisible = true;
    }

    /**
     * Hide .collapse-content if visibile
     */
    hide() {
        if (!this.isVisible)
            return;

        if (this.hasTransition)
            this.handleTransitionOut();
        
        this.content.classList.remove('is-visible');
        this.content.setAttribute('aria-expanded', false);
        this.isVisible = false;
    }


    /**
     * Handle animations for expand
     */
    handleTransitionIn() {
        // Remove class after animation is done
        this.content.addEventListener('transitionend', () => {
            this.content.classList.remove('is-transitioning-in')
        }, {once: true});

        // Init animation
        this.content.classList.add('is-transitioning-in');
    }

    /**
     * Handle animations for collapse
     */
    handleTransitionOut() {
        // Set height to enable full vertical transition
        this.content.style.height = `${this.content.getBoundingClientRect().height}px`;

        // Force reflow to make animation work
        reflow(this.content);

        // Remove class after animation is done
        this.content.addEventListener('transitionend', () => {
            this.content.classList.remove('is-transitioning-out')
        }, {once: true});

        // Init animation
        this.content.classList.add('is-transitioning-out');
        this.content.style.height = '';
    }
}


Bulma.registerPlugin('collapse', Collapse);

export default Collapse;
