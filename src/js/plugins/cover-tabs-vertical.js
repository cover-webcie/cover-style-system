import Bulma from '@vizuaalog/bulmajs/src/core';


class CoverTabsVertical {
    /**
     * Handle parsing the DOMs data attribute API.
     * @param {HTMLElement} element The root element for this instance
     * @return {undefined}
     */
    static parseDocument(context) {
        this.scrollPos = window.scrollY;
        const elements = context.querySelectorAll('.tabs-wrapper.is-vertical');
        Bulma.each(elements, element => {
            new CoverTabsVertical({
                element: element,
                history: (element.dataset.history != null),
            });
        });
    }

    constructor(options) {
        this.isActive = false;
        this.element = options.element;
        this.history = options.history;

        this.element.classList.add('is-enriched');

        this.sidebar = this.element.querySelector('.column.is-sidebar');
        this.content = this.element.querySelector('.column.is-content');
        this.backButton = this.element.querySelector('.tabs-back');

        this.handleResize();
        this.setupEvents();
    }

    setup() {
        this.isActive = true;
        this.backButton.hidden = false;

        if (window.location.hash !== '' && this.content.querySelector('.tabs-content ' + window.location.hash))
            this.showContent();
        else
            this.showTabs();

    }

    suspend() {
        this.isActive = false;
        this.backButton.hidden = true;
        this.sidebar.hidden = false;
        this.content.hidden = false;
    }

    setupEvents() {
        this.sidebar.querySelectorAll('.tabs li a').forEach(tab => {
            tab.addEventListener('click', this.showContent.bind(this));
        });

        this.backButton.addEventListener('click', this.showTabs.bind(this));

        window.addEventListener('resize', this.handleResize.bind(this));
    }

    showTabs() {
        if (!this.isActive)
            return;

        this.sidebar.hidden = false;
        this.content.hidden = true;
        this.backButton.classList.remove('is-stuck');
        window.scrollTo({
            top: this.scrollPos,
            behavior: 'auto'
        });

        if (this.history)
            history.replaceState(null, '', window.location.pathname + window.location.search);
    }

    showContent() {
        if (!this.isActive)
            return;

        this.scrollPos = window.scrollY;
        this.sidebar.hidden = true;
        this.content.hidden = false;
    }

    handleResize() {
        const viewport = String(getComputedStyle(document.documentElement).getPropertyValue('--viewport-name')).trim();

        if (viewport === 'mobile' || viewport === 'tablet')
            this.setup();
        else
            this.suspend();
    }
}

if (!window.coverTabsVerticalInitialized) {
    CoverTabsVertical.parseDocument(document);
    document.addEventListener('partial-content-loaded', event => CoverTabsVertical.parseDocument(event.detail));
    window.coverTabsVerticalInitialized = true;
}

export default CoverTabsVertical;
