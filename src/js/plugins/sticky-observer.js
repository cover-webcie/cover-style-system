import Bulma from '@vizuaalog/bulmajs/src/core';

const ZERO_POSITION_FIX = 2; // 1 doesn't always work in Firefox, 2 works everywhere (always)
const ZERO_POSITION_FIX_ROOT_MARGIN = 1; // Should be 0 if FIX is set to 1
const EPSILON = 1;

/**
 * StickyObserver plugin to detect whether position: sticky element is stuck.
 * Supports the following data options:
 *
 * sticky        = Boolean attribute to activate the plugin. Alternatively,
 *                 .is-sticky can be added.
 * sticky-ignore = Boolean attribute tot add to a .is-sticky to prevent activation
 * sticky-root   = Selector to find the ancestor element used as root by
 *                 IntersectionObserver. (default: "viewport", but if the element
 *                 is a th or td, it tries to find the closest .table-container.
 *                 This can be overridden by provinding "viewport" as root).
 * sticky-class  = Class to add to stuck elements. (default: "is-stuck")
 *
 * Takes some inspiration from https://stackoverflow.com/a/61115077 [1] and 
 * https://stackoverflow.com/a/57991537 [2], with a few additional workarounds.
 * The [1] technique doesn't appear to work for position values of 0px when the 
 * extreme end of their viewport is the only position where the element is not 
 * stuck Fallback to [2].
 */
class StickyObserver {
    static parseDocument(context) {
        const elements = context.querySelectorAll('.is-sticky:not([data-sticky-ignore]),[data-sticky]');

        Bulma.each(elements, element => {
            new StickyObserver({
                element: element,
                root: element.dataset.stickyRoot || null,
                stuckClass: element.dataset.stickyClass || 'is-stuck',
            });
        });

    }

    constructor(options) {
        this.element = options.element;
        this.stuckClass = options.stuckClass;
        this.options = options;

        // Only initialise if we're not hidden
        if (this.element.hidden)
            this.initHiddenObserver();
        else
            this.init();
    }

    init() {
        this.initPositions(this.options);
        this.initObserver(this.options);
        window.addEventListener('resize', this.handleResize.bind(this));
    }

    initPositions(options) {
        const style = getComputedStyle(options.element);
        this.positions = {};

        // Detect in which directions the element is intended to be stuck
        this.initPosition(style, 'top');
        this.initPosition(style, 'right');
        this.initPosition(style, 'bottom');
        this.initPosition(style, 'left');
    }

    initPosition(style, direction) {
        this.positions[direction] = null;
        if (style[direction] !== 'auto') {
            this.positions[direction] = parseInt(style[direction], 10);
            // Fallback to [2].
            if (this.positions[direction] === 0) {
                this.element.style[direction] = `-${ZERO_POSITION_FIX}px`;
            }
        } 
    }

    handleResize() {
        // Reset on resize, as mediaqueries may have changed positioning
        if (this.observer)
            this.observer.disconnect();
        this.initPositions(this.options);
        this.initObserver(this.options);
    }

    handleObservation(entries, observer) {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                let element = entry.target;
                let isStuck = false;

                // Determine stuck value for each direction
                if (this.positions.top !== null)
                    isStuck = isStuck || Math.abs(entry.intersectionRect.top - entry.rootBounds.top) < EPSILON;
                if (this.positions.right !== null)
                    isStuck = isStuck || Math.abs(entry.intersectionRect.right - entry.rootBounds.right) < EPSILON;
                if (this.positions.bottom !== null)
                    isStuck = isStuck || Math.abs(entry.intersectionRect.bottom - entry.rootBounds.bottom) < EPSILON;
                if (this.positions.left !== null)
                    isStuck = isStuck || Math.abs(entry.intersectionRect.left - entry.rootBounds.left) < EPSILON

                // We're only stuck if not fully visible
                isStuck = isStuck && entry.intersectionRatio < 1;

                // Apply class
                element.classList.toggle(this.stuckClass, isStuck);
            }
        });
    }

    getMargin(m) {
        if (m === null)
            return 0;
        // Fallback to [2].
        if (m === 0)
            return ZERO_POSITION_FIX_ROOT_MARGIN;
        return -1 * Math.abs(m + 1);
    }

    initObserver(options) {
        // Prepare observer options
        const margins = [
            `${this.getMargin(this.positions.top)}px`,
            `${this.getMargin(this.positions.right)}px`,
            `${this.getMargin(this.positions.bottom)}px`,
            `${this.getMargin(this.positions.left)}px`,
        ];

        let observerOptions = {
            rootMargin: margins.join(' '),
            threshold: [.9, 1], // Chromium doesn't (always) work with 1 alone.
        };

        // Figure out viewport root, fallback to "null" which is the main viewport
        if (options.root !== null && options.root !== 'viewport')
            observerOptions.root = options.element.closest(options.root);
        else if (this.element.tagName.toLowerCase() === 'td' || this.element.tagName.toLowerCase() === 'th')
            observerOptions.root = options.element.closest('.table-container');

        // Init observer
        this.observer = new IntersectionObserver(this.handleObservation.bind(this), observerOptions);
        this.observer.observe(this.element);
    }

    initHiddenObserver(options) {
        // Make sure we initialise after we've become visible
        let observerOptions = {
            childList: false,
            attributes: true,
        }
        let observer = new MutationObserver((mutationsList, observer) => {
            for (let mutation of mutationsList) {
                if (mutation.type === 'attributes' && mutation.attributeName === 'hidden' && !this.element.hidden)
                    this.init();
            }
        });
        observer.observe(this.element, observerOptions);
    }
}

if (!window.stickyObserverInitialized) {
    StickyObserver.parseDocument(document);
    document.addEventListener('partial-content-loaded', event => StickyObserver.parseDocument(event.detail));
    window.stickyObserverInitialized = true;
}

export default StickyObserver;
