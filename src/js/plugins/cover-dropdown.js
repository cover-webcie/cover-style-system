import Bulma from '@vizuaalog/bulmajs/src/core';
import Dropdown from '@vizuaalog/bulmajs/src/plugins/dropdown';


class CoverDropdown extends Dropdown {
    /**
     * Helper method used by the Bulma core to create a new instance.
     * @param  {Object} options The options object for this instance
     * @returns {Tabs} The newly created instance
     */
    static create(options) {
        return new CoverDropdown(options);
    }

    static parse(element) {
        new CoverDropdown({
            element: element
        });
    }

    registerEvents() {
        document.addEventListener('click', this.handleDocumentClick.bind(this));
        return super.registerEvents();
    }

    handleDocumentClick(event) {
        if(this.element.classList.contains('is-active') && !this.element.contains(event.target)) {
             this.element.classList.remove('is-active');
        }
    }
}

Bulma.registerPlugin('dropdown', CoverDropdown, 100000)

export default CoverDropdown;
