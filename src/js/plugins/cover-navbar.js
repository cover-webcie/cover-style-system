import Bulma from '@vizuaalog/bulmajs/src/core';
import Navbar from '@vizuaalog/bulmajs/src/plugins/navbar';


class CoverNavbar extends Navbar {
    /**
     * Handle parsing the DOMs data attribute API.
     * @param {HTMLElement} element The root element for this instance
     * @return {undefined}
     */
    static parse(element) {
        new CoverNavbar({
            element: element,
            sticky: element.hasAttribute('data-sticky') ? true : false,
            stickyOffset: element.hasAttribute('data-sticky-offset') ? element.getAttribute('data-sticky-offset') : 0,
            hideOnScroll: element.hasAttribute('data-hide-on-scroll') ? true : false,
            tolerance: element.hasAttribute('data-tolerance') ? element.getAttribute('data-tolerance') : 0,
            shadow: element.hasAttribute('data-sticky-shadow') ? true : false
        });
    }

    /**
     * Plugin constructor
     * @param  {Object} options The options object for this plugin
     * @return {this} The newly created plugin instance
     */
    constructor(options) {
        super(options);

        this.dropdowns = [];

        const burgerTrigger = this.element.querySelector('.navbar-burger');
        if (burgerTrigger )
            this.dropdowns.push({
                trigger: burgerTrigger,
                target: this.element.querySelector('.navbar-menu'),
                type: 'burger',
            });

        for (let element of this.element.querySelectorAll('.navbar-item.has-dropdown')) {
            this.dropdowns.push({
                trigger: element.querySelector('.navbar-link'),
                target: element,
                type: 'dropdown',
            });            
        }

        this.initSearch();

        // Fix for registerEvents call in super constructor
        this.initialized = true;
        this.registerEvents();
    }

    /**
     * Register all the events this module needs.
     * @return {undefined}
     */
    registerEvents() {
        if (!this.initialized) {
            // Fix for registerEvents call in super constructor
            return;
        }

        window.addEventListener('resize', this.handleResize.bind(this));
        this.handleResize();

        document.addEventListener('click', this.handleHideAllDropdowns.bind(this));

        for (let dropdown of this.dropdowns) {
            dropdown.trigger.addEventListener('click', this.handleTriggerDropdown(dropdown).bind(this));

            if (dropdown.type === 'dropdown' && dropdown.target.classList.contains('is-hoverable')) {
                dropdown.trigger.addEventListener('mouseenter', this.handleHideAllDropdowns.bind(this));
            } else if (dropdown.type === 'dropdown' && dropdown.target.classList.contains('is-hoverable-desktop')) {
                dropdown.trigger.addEventListener('mouseenter', (evt) => {
                    if (this.viewport !== 'mobile' && this.viewport !== 'tablet') {
                        this.handleHideAllDropdowns(evt);
                    }
                });
            }
        }

        if(this.sticky) {
            window.addEventListener('scroll', this.handleScroll.bind(this));
        }
    }

    /**
     * Handle the dropdown toggle events.
     * @return {undefined}
     */
    handleTriggerDropdown(dropdown) {
        return function (evt) {
            if (dropdown.target.classList.contains('is-active')) {
                dropdown.target.classList.remove('is-active');
                dropdown.trigger.classList.remove('is-active');
            } else {
                dropdown.target.classList.add('is-active');
                dropdown.trigger.classList.add('is-active');
            }
        };
    }

    /**
     * Handle the dropdown show events.
     * @return {undefined}
     */
    handleShowDropdown(dropdown) {
        return function (evt) {
            dropdown.target.classList.add('is-active');
            dropdown.trigger.classList.add('is-active');
        };
    }

    /**
     * Handle the dropdown hide all events.
     * @return {undefined}
     */
    handleHideAllDropdowns (evt) {
        for (let dropdown of this.dropdowns) {
            if(!dropdown.target.contains(evt.target) && !dropdown.trigger.contains(evt.target)) {
                dropdown.target.classList.remove('is-active');
                dropdown.trigger.classList.remove('is-active');
                dropdown.trigger.blur();
            }
        }
    }

    /**
     * Handle clicking the search button.
     * Only execute default action (submit) when the input is not empty. Otherwise, focus input.
     */
    handleSearchButtonClick (searchForm) {
        return (evt) => {
            let input = searchForm.querySelector('input');
            if (!input.value.trim()) {
                evt.preventDefault();
                input.focus();
            }
        };
    }

    handleResize() {
        this.viewport = String(getComputedStyle(document.documentElement).getPropertyValue('--viewport-name')).trim();
    }

    /**
     * Initialise search button actions.
     */
    initSearch() {
        const forms = this.element.querySelectorAll('.search-form')

        for (let form of forms) {
            form.querySelector('button[type=submit]').addEventListener('click', this.handleSearchButtonClick(form));
        }
    }
}

Bulma.registerPlugin('navbar', CoverNavbar, 100);

export default CoverNavbar;
