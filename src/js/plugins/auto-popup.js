import Bulma from '@vizuaalog/bulmajs/src/core';
import Modal from '@vizuaalog/bulmajs/src/plugins/modal';


class AutoPopupModal extends Modal {

    constructor(options={}) {
        options.style = 'auto-popup';
        options.closable = false;
        super(options);

        this.loadingTimeout = setTimeout(this.createLoading.bind(this), this.option('spinnerDelay', .75 * 1000));
        this.closeButton = Bulma.createElement('button', 'is-hidden');

        this.open();
    }

    reset(style, closable) {
        clearTimeout(this.loadingTimeout);
        this.element.classList.remove('is-loading-content');

        if (closable !== undefined)
            this.closable = closable;
        else
            this.closable = this.originalOptions.closable;

        let newContent = Bulma.createElement('div', style === 'card' ? 'modal-card' : 'modal-content');
        this.content.replaceWith(newContent);
        this.content = newContent;

        // Create mock button to handle setupEvents correctly
        this.closeButton = Bulma.createElement('button', 'is-hidden');

        if (this.closable && style !== 'card')
            Bulma.findOrCreateElement('.modal-close', this.element, 'button');
    }

    getSpinner(wrapperClasses=[]) {
        if(typeof wrapperClasses === 'string')
            wrapperClasses = [wrapperClasses];

        let icon = Bulma.createElement('i', ['fas', 'fa-circle-notch', 'fa-spin', 'fa-3x']);
        let iconWrapper = Bulma.createElement('span', ['icon', 'is-large', 'level-item', ...wrapperClasses]);
        iconWrapper.append(icon);

        return iconWrapper;
    }

    createLoading() {
        let spinnerWrapper = Bulma.createElement('div', 'level');
        spinnerWrapper.append(this.getSpinner('has-text-light'));

        this.content.innerHTML = '';
        this.content.append(spinnerWrapper);
        this.element.classList.add('is-loading-content');

        this.loadingTimeout = setTimeout(this.createLongLoading.bind(this), this.option('longLoadingDelay', 3 * 1000));
    }

    createLongLoading() {
        let message = Bulma.createElement('p', ['has-text-centered', 'has-text-white', 'level-item']);
        message.innerHTML = this.option('longLoadingMessage', 'Loading takes longer than expected…');

        let messageWrapper = Bulma.createElement('div', 'level');
        messageWrapper.append(message);

        let closeButton = Bulma.createElement('button', ['button', 'is-small', 'is-danger', 'is-inverted', 'is-outlined']);
        closeButton.dataset.dismiss = 'modal';
        closeButton.innerHTML = 'Cancel';

        // Make close button cancel everything without enabling closing by clicking the background
        closeButton.addEventListener('click', this.close.bind(this));

        let buttonLevelItem = Bulma.createElement('div', ['level-item', 'has-text-centered']);
        let buttonWrapper = Bulma.createElement('div', 'level');
        buttonLevelItem.append(closeButton);
        buttonWrapper.append(buttonLevelItem);

        this.content.append(messageWrapper);
        this.content.append(buttonWrapper);
    }

    createCard(title, body, buttons=[], colorClass='') {
        this.reset('card', true);
        this.title = title;
        this.body = body;
        this.createCardStructure();

        if (colorClass) {
            this.header.classList.add(colorClass);
            this.headerTitle.classList.add('has-text-white');
        }

        // Create close button if it doesn't exist
        this.closeButton = Bulma.findOrCreateElement('.delete', this.header , 'button');

        // Initialize footer buttons
        if (buttons.length > 0)
            this.options.buttons = buttons;
        else
            this.options.buttons = [{
                label: 'OK',
                classes: ['button'],
                onClick: this.close.bind(this)
            }]

        this.createButtons();
        
        document.dispatchEvent(new CustomEvent('partial-content-loaded', { bubbles: true, detail: this.content }));
        this.setupEvents();
    }

    createImage(img) {
        this.reset('image', true);

        let imgElement = new Image();
        imgElement.src = img;
        this.content.append(imgElement);

        this.setupEvents();
    }

    createFromModalCard(card) {
        this.reset('card', true);

        let header = card.querySelector('.modal-card-head');        

        // Create close button if it doesn't exist
        if (header)
            Bulma.findOrCreateElement('.delete', header , 'button');
        else
            Bulma.findOrCreateElement('.modal-close', this.element, 'button');
        
        this.content.replaceWith(card);
        this.content = card;

        document.dispatchEvent(new CustomEvent('partial-content-loaded', { bubbles: true, detail: this.content }));
        this.setupEvents();
    }

    close() {
        super.close();

        // Remove modal from body
        this.destroy();
    }

    setupEvents() {
        if (this.closable) {
            for (let button of this.element.querySelectorAll('.delete, .modal-close, [data-dismiss=modal]')) {
                button.addEventListener('click', evt => {
                    evt.preventDefault();
                    this.close();
                });
            }
        }

        super.setupEvents();
    }
}

class AutoPopup {
    static parseDocument(context) {
        let elements = context.querySelectorAll('a[data-popup]');

        Bulma.each(elements, (element) => {
            new AutoPopup({
                element: element,
                contentType: element.dataset.popup,
                // no url, this will get it from the href attr
            });
        });
    }

    /**
     * Plugin constructor
     * @param  {Object} options The options object for this plugin
     * @return {this} The newly created plugin instance
     */
    constructor(options, response=null) {
        this.options = options;
        this.contentType = options.contentType;
        this.cancelled = false;

        if (response) {
            this.modal = new AutoPopupModal({
                onClose: this.handleClose.bind(this),
            });

            this._preprocessContent(response)
                .then(this.loadContent.bind(this))
                .catch(this.loadError.bind(this));
        } else {
            this.element = options.element;
            this.url = options.url;
            this.registerEvents();
        }
    }

    /**
     * Register all the events this module needs.
     * @return {undefined}
     */
    registerEvents() {
        this.element.addEventListener('click', this.handleClick.bind(this));
    }

    handleClick(evt) {
        if (evt.shiftKey || evt.ctrlKey || evt.metaKey)
            return;

        evt.preventDefault();

        this.cancelled = false;

        this.modal = new AutoPopupModal({
            onClose: this.handleClose.bind(this),
        });

        this.fetchContent()
            .then(this.loadContent.bind(this))
            .catch(this.loadError.bind(this));

    }

    handleClose(modal) {
        this.cancelled = true;
    }

    fetchContent() {
        let url = this.url;
        if (!url)
            url = this.element.getAttribute('href');

        return this._preprocessContent(fetch(url));
    }

    async _preprocessContent(prom) {
        const response = await prom;

        if (!response.ok) {
            throw new Error(await response.text());
        }

        return await response.blob();
    }

    loadContent(blob) {
        if (this.cancelled)
            return;

        if (this.contentType === 'modal' && blob.type.startsWith('text/html')) {
            blob.text().then(result => {
                let doc = (new DOMParser()).parseFromString(result, 'text/html');
                let modal = doc.querySelector('.modal .modal-card');
                this.modal.createFromModalCard(modal);
            }).catch(this.loadError.bind(this));
        } else if (this.contentType === 'image' && blob.type.startsWith('image')) {
            this.modal.createImage(URL.createObjectURL(blob));
        } else {
            throw new Error('Unexpected content loaded. Please contact the administrator.');
        }
    }
    
    loadError(message) {
        if (this.cancelled)
            return;

        if(!this.options.hasOwnProperty('errorTitle') || this.options['errorTitle'] === null)
            var title = 'Oops! something went wrong…';
        else
            var title = this.options.errorTitle;

        this.modal.createCard(
            title,
            message,
            [],
            'has-background-danger'
        )
    }
}

if (!window.autoPopupInitialized) {
    AutoPopup.parseDocument(document);
    document.addEventListener('partial-content-loaded', event => AutoPopup.parseDocument(event.detail));
    window.autoPopupInitialized = true;
}

export default AutoPopup;
